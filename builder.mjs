import path from "path"
import tar from "tar"
import fs from "fs"
import {File, Folder, SiteGenerator, Template} from "./kickstand/index.mjs"
const __dirname = path.resolve()
const layoutsFolder = path.join(__dirname, "/templates/layouts")
const pagesFolder = path.join(__dirname, "/templates/pages")
const PUBLIC_FOLDER = path.resolve(__dirname, "./public")
const BLOG_INDEX_TEMPLATE_PATH = `${pagesFolder}/blog/index.html`
const fsAsync = fs.promises

const builder = {
    createFolders(){
        Promise.all([Folder.create("templates"),
            Folder.create("templates/layouts"),
            Folder.create("templates/pages"),
            Folder.create("public")]
        ).then(v=>{
            console.log("Folders created")
        }).catch(e => {
            if(e.code != "EEXIST")console.error("Creating folders: ", e)
        })
        return SiteGenerator
    },
    async copyStaticResoucesOver(){
        await Folder.copy(path.resolve(__dirname, "./templates/images"), `${PUBLIC_FOLDER}/images`)
    },
    async cleanupBeforeStarting(){
        await Folder.delete(`${__dirname}/public/blog`)
    },
    async createBlogIndexTemplateWithPosts(posts) {
        let html = ""
        try{
            let f = await File.read(BLOG_INDEX_TEMPLATE_PATH)
            let obj = SiteGenerator.renderPageObject(BLOG_INDEX_TEMPLATE_PATH, f.data)
            html = obj.template(posts.reverse())
        } catch(e){
            console.log("Creating Blog Index Template with posts: ", e)
        }
        return html
    },
    async run(){
        try{
            await this.cleanupBeforeStarting()
        }catch(e){
            console.error("Cleanup before start", e)
        }
        const posts = []
        const blogPostPattern = /\/blog\/\d+/
        let files = await SiteGenerator.run(pagesFolder, layoutsFolder)
        for(let i = 0; i < files.length; i++){
            let t = files[i]
            let newFilePath = t.file.replace(pagesFolder, "")
            let context = {
                meta: {
                    uri: newFilePath
                }
            }
            try{
                let b = await File.write(path.join(__dirname, "/public/", newFilePath), t.template(context))
                if(blogPostPattern.test(t.file) && context.meta.should_publish === "yes"){
                    posts.push(context.meta)
                }
            } catch(e){
                console.error("Filling posts[]: ", t.file, e)
            }
        }
        let output = await this.createBlogIndexTemplateWithPosts(posts)
        await File.write(path.join(__dirname, "/public/blog/", "index.html"), output)
        const images = await Folder.read(`${__dirname}/templates/images/`)
        try{await Folder.create(`${__dirname}/public/images`)}catch(e){}
        //fs.createReadStream(`${__dirname}/RockIT-logo/logo.png`).pipe(fs.createWriteStream(`${__dirname}/public/images/logo.png`))
        images.forEach(image=>{
            let fileName = image.replace(`${__dirname}/templates/images/`, "")
            fs.createReadStream(image).pipe(fs.createWriteStream(`${__dirname}/public/images/${fileName}`))
        })
    }
}

export default builder