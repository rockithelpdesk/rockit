import Path from "path"
import tdding from "./lib/Tdding.mjs"
import glob from "glob"
const __dirname = Path.resolve()
const args = Object.assign([], process.argv)
args.shift()
args.shift()

process.on("shouldStart", async files => {
    let tests = []
    for await(let file of files){
        let t = await import(Path.join(__dirname, file))
        tests = tests.concat(t)
    }
    await tdding.run()
})

if(args.length == 1){
    glob(args[0], (err, files)=>{
        if(err) return console.log(err)
        process.emit("shouldStart", files)
    })
} else {
    process.emit("shouldStart", args)
}