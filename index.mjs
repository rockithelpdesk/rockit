import express from "express"
import http from "http"
import builder from "./builder.mjs"
import build from "./build.mjs"
const app = express()
const port = process.env.PORT || 3000
builder.createFolders()

app.use(express.static("public", {
    setHeaders: (res, path) => {
        if(path.indexOf(".well-known") === -1) return
        res.setHeader("Content-Type", "text/plain")
    }
}))

app.get("/health", (req, res)=>{
    res.send("Ok")
})

http.createServer(app).listen(port, () => {
    console.log(`Listening on http://localhost:${port}`);
})

Promise.all([
    build()
]).then(v=>{
    console.log("Folders created")
}).catch(e => {
    if(e.code != "EEXIST")console.error("Creating folders: ", e)
})
