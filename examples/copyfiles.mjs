import tdding from "../lib/Tdding.mjs"
import path from "path"
import Folder from "../lib/Folder.mjs"

const __dirname = path.resolve()
tdding.push("copy files from templates to public", async t=>{
    const SOURCE_FOLDLER = path.resolve(__dirname, "./templates/images/")
    const TARGET_FOLDER = path.resolve(__dirname, "./public/images/")
    const files = await Folder.read(SOURCE_FOLDLER)
    const copiedFiles = await Folder.copy(SOURCE_FOLDLER, TARGET_FOLDER)
    console.log(copiedFiles)
    copiedFiles.forEach((f, i)=>{
        t.ok(f.indexOf(files[i].split("/").pop() > -1), `The files should've been copied: ${files[i]}`)
    })
})

export default tdding