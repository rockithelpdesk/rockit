import tdding from "../lib/Tdding.mjs"
import Folder from "../lib/Folder.mjs"
import Fs from "fs"
import Path from "path"
const File = Fs.promises
const __dirname = Path.resolve()
function tempFolder() {
    return Path.join(__dirname, "examples/temp")
}
tdding.on("finished", async ()=>{
    try{
        await Folder.delete(tempFolder())
    }catch(e){
        
    }
})
tdding.on("starting", async ()=>{
    await Folder.create(tempFolder())
})
tdding.push("Should read all files in a folder", async t => {
    await File.writeFile(Path.resolve(tempFolder(), "test.txt"), "Some text")
    const files = await Folder.read(tempFolder())
    t.ok(files.length == 1, "There should be 1 file in here")
})
export default tdding
