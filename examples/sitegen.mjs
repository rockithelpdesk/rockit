import tdding from "../lib/Tdding.mjs"
import Path from "path"
import Site from "../lib/Site.mjs"
const __dirname = Path.resolve()

tdding.push("Take list of folders and generate new files", async t => {
    let site = new Site({
        paths: [
            Path.join(__dirname, "examples", "another_html_folder"),
            Path.join(__dirname, "examples", "home")
        ],
        layouts: Path.join(__dirname, "examples", "layouts")
    })
    const files = await site.gen({
        meta: {
            title: "Meow"
        },
        products: [
            {
                name: "Some product name",
                price: 1.23
            }
        ]
    }, {
        shouldIncludeFile(fileName){
            return fileName.indexOf(".phtml") > -1
        }
    })
    t.ok(files.some(file=>file.fileName.indexOf(".html") >- 1), "Should contain html files.")
    t.ok(!files.some(file=>file.fileName.indexOf(".js") >-1), "Should be able to filter out what files are included.")
    t.ok(files.some(file=>file.data.indexOf("<!DOCTYPE") > -1), "Should be a real HTML document.")
    t.ok(files.some(file=>file.data.indexOf("<title>Meow") > -1), "Should interpolate the model attributes into the template.")
    t.ok(files.find(file=>file.fileName.indexOf("another_html_folder/index") > -1).data.indexOf("Some product name") > -1, "Should interpolate another attribute in the context object.")
    t.ok(files.find(file=>file.fileName.indexOf("another_html_folder/index") > -1).data.indexOf(1.23) > -1, "Should interpolate price.")
})

export default tdding