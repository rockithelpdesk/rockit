import Dir from "./Dir.mjs"
import SiteGenerator from "./SiteGenerator.mjs"
import Template from "./Template.mjs"

const File = Dir.File
const Folder = Dir.Folder
export { File, Folder, SiteGenerator, Template }
