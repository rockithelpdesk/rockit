import fs, { copyFile } from "fs"
import path from "path"
import mkdirp from "mkdirp"
const fsAsnyc = fs.promises
const File = {
    async read(f){
        let data = await fsAsnyc.readFile(f, {encoding: "utf-8"})
        return {file: f, data}
    },
    async unlink(filePath){
        return await fsAsnyc.unlink(filePath)
    },
    async write(f, data){
        return new Promise((resolve, reject)=>{
            let rootFolder = f.split(path.sep)
            rootFolder.pop()
            rootFolder = rootFolder.join(path.sep)
            mkdirp(rootFolder, (err, createdFolder) => {
                if (err){
                    return reject(err)
                }
                fs.writeFile(f, data, err => {
                    if(err) {
                        return reject(err)
                    }
                    resolve({file: f, data: data})
                })
            })
        })
    },
    async stat(f){
        return await fsAsnyc.stat(f)
    }
}
const readdir = async (folder)=>{
    return await fsAsnyc.readdir(folder)
}
const rmdir = async folder => {
    return await fsAsnyc.rmdir(folder)
}
const Folder = {
    async read(folder){
        let files = await readdir(folder)
        let justFiles = []
        let upper = files.length
        for(let i = 0; i < upper; i++){
            let f = path.join(folder, files[i])
            let stat = await File.stat(f)
            if(stat.isDirectory()){
                justFiles = justFiles.concat(await Folder.read(f))
            } else {
                justFiles.push(f)
            }
        }
        return justFiles
    },
    async copy(source, target){
        const files = await readdir(source)
        const upper = files.length
        const copiedFiles = []
        for(let i = 0; i < upper; i++){
            const f = path.join(source, files[i])
            let stat = await File.stat(f)
            const childTarget = `${target}/${files[i]}`
            if(stat.isDirectory()){
                await this.create(childTarget)
                await this.copy(f, childTarget)
            } else {
                await fsAsnyc.copyFile(f, childTarget)
                copiedFiles.push(childTarget)
            }
        }
        return copiedFiles
    },
    async delete(folderPathName){
        let files = await this.read(folderPathName)
        let upper = files.length
        for(let i = 0; i < upper; i++){
            await File.unlink(files[i])
        }
        let folders = await readdir(folderPathName)
        let foldersMax = folders.length
        for(let i = 0; i < foldersMax; i++){
            try{
                await rmdir(`${folderPathName}/${folders[i]}`)
            }catch(e){console.log(e)}
        }
    },
    async create(folder){
        return await fsAsnyc.mkdir(folder)
    }
}

export default {
    File: File,
    Folder: Folder
}