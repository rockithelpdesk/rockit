import handlebars from "handlebars"
import MarkdownIt from "markdown-it"
import Meta from "markdown-it-meta"
const Template = {
    get(extension, data){
        let md = new MarkdownIt({
            html: true,
            linkify: true
        })
        md.use(Meta)
        if(extension.indexOf("md") > -1){
            return (context)=>{
                let output = md.render(data)
                if(md.meta.layout){
                    output = `{{#> ${md.meta.layout}}}\n${output}{{/${md.meta.layout}}}`
                    context.meta = Object.assign(md.meta, context.meta)
                }
                let template = handlebars.compile(output)
                return template(context)
            }
        }
        return handlebars.compile(data)
    },
    transformToHtml(fileName){
        return fileName.replace("md", "html")
    }
}
export default Template