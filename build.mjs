import builder from "./builder.mjs"
const build = async ()=>{
    await builder.copyStaticResoucesOver()
    await builder.run()
}
export default build