import EventEmitter from "events"
import { setTimeout } from "timers"
//console.clear()
const colors = {
    reset: "\x1b[0m",
    bright:"\x1b[1m",
    dim: "\x1b[2m",
    underscore: "\x1b[4m",
    blink: "\x1b[5m",
    reverse: "\x1b[7m",
    hidden: "\x1b[8m",
    fgBlack: "\x1b[30m",
    fgRed: "\x1b[31m",
    fgGreen: "\x1b[32m",
    fgYellow: "\x1b[33m",
    fgBlue: "\x1b[34m",
    fgMagenta: "\x1b[35m",
    fgCyan: "\x1b[36m",
    fgWhite: "\x1b[37m",
    bgBlack: "\x1b[40m",
    bgRed: "\x1b[41m",
    bgGreen: "\x1b[42m",
    bgYellow: "\x1b[43m",
    bgBlue: "\x1b[44m",
    bgMagenta: "\x1b[45m",
    bgCyan: "\x1b[46m",
    bgWhite: "\x1b[47m"
}

const subscribers = []
let counter = 0
class Tdd extends Array {
    constructor(...args){
        super(...args)
    }
    on(eventName, listener){
        subscribers.push({eventName, listener})
    }
    off(eventName, listener){
        let index = -1
        for(let i = 0; i < subscribers.length; i++){
            if(subscribers[i].eventName == eventName
                && subscribers[i].listener === listener){
                index = i
                break
            }
        }
        if(index != -1){
            subscribers = subscribers.splice(index, 1)
        }
    }
    ok(check, message){
        counter++
        if(check){
            console.log(`${counter}. ${colors.fgGreen}${message}${colors.reset}`)
          } else {
            console.log(`${counter}. ${colors.bgRed}${message}${colors.reset}`)
        }
    }
    async skip(description, fn){

    }
    async push(description, fn){
        super.push({description, fn})
    }
    async run(){
        subscribers.forEach(s=>{
            if(s.eventName == "starting") s.listener()
        })
        console.time("tdding")
        for await (let example of this){
            try{
                await example.fn(this)
            }catch(e){
                console.log(` ${counter}. ${colors.bgRed}${example.description}${colors.reset}`, e)
            }
        }
        console.timeEnd("tdding")
        subscribers.forEach(s=>{
            if(s.eventName == "finished") s.listener()
        })
    }
}

const Tdding = new Tdd()
if(process.argv.length < 2 || process.argv[1].indexOf("test.mjs") == -1){
    setImmediate(async () => {
        await Tdding.run()
    })    
}
export default Tdding
