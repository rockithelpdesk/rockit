import Fs from "fs"
import Path from "path"
const File = Fs.promises
const Folder = {
    async read(folder){
        let files = await File.readdir(folder)
        let justFiles = []
        let upper = files.length
        for(let i = 0; i < upper; i++){
            let f = Path.join(folder, files[i])
            let info = await File.stat(f)
            if(info.isDirectory()){
                justFiles = justFiles.concat(await this.read(f))
            } else {
                justFiles.push(f)
            }
        }
        return justFiles
    },
    async copy(fromFileName, toFileName){
        console.log(fromFileName, toFileName)
        const folders = toFileName.split(Path.sep)
        folders.pop()
        let folderPath = "/"
        let files = []
        for(let i = 0; i < folders.length; i++){
            folderPath = Path.resolve(folderPath, folders[i], Path.sep)
            try{
                await File.stat(folderPath)
            }catch(e){
                try{
                    console.log("folderPath", folderPath)
                    await this.create(folderPath)
                    files.push(folderPath)
                }catch(e){

                }
            }
        }
        Fs.createReadStream(fromFileName).pipe(Fs.createWriteStream(toFileName))
        //.on("error", e => console.log(e))
        //.on("finish", ()=> console.log("closing"))
        return files
    },
    async delete(folderPathName){
        let files = await this.read(folderPathName)
        let upper = files.length
        for(let i = 0; i < upper; i++){
            await File.unlink(files[i])
        }
        let folders = await File.readdir(folderPathName)
        let foldersMax = folders.length
        for(let i = 0; i < foldersMax; i++){
            try{
                await File.rmdir(`${folderPathName}/${folders[i]}`)
            }catch(e){console.log(e)}
        }
        await File.rmdir(folderPathName)
    },
    async create(folder){
        await File.mkdir(folder)
    }
}
export default Folder