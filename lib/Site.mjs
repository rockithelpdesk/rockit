import Folder from "../lib/Folder.mjs"
import Fs from "fs"
import Path from "path"
import Handlebars from "handlebars"
const File = Fs.promises
class SiteFile {
    constructor({
        fileName,
        data
    }){
        this.fileName = fileName
        this.data = data
    }
}
class Site {
    constructor({
        paths,
        layouts
    }){
        this.paths = paths
        this.layouts = layouts
    }
    async gen(model, delegate){
        delegate = delegate || {
            shouldIncludeFile(fileName){
                return true
            }
        }
        const layoutFiles = await Folder.read(this.layouts)
        for await(let fileName of layoutFiles){
            let data = await File.readFile(fileName, {encoding: "utf8"})
            Handlebars.registerPartial(fileName.split(Path.sep).pop(), data)
        }

        const files = []
        for await (let folder of this.paths){
            let filesInFolder = await Folder.read(folder)
            for await (let fileName of filesInFolder){
                if(!delegate.shouldIncludeFile(fileName)) continue

                let data = await File.readFile(fileName, {encoding: "utf8"})
                let template = Handlebars.compile(data)
                files.push(new SiteFile({fileName: fileName.replace(".phtml", ".html"), data: template(model)}))
            }
        }
        return files
    }
}

export default Site
export {Site, SiteFile}